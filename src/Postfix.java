/*
 * Original work Copyright (c) 2014 TranIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Project: csap-stacks
 * File: Postfix.java
 */

/*
* QOTP:
*   "C makes it easy to shoot yourself in the foot; C++ makes it harder, but when you do,
*   it blows away your whole leg."
*       -Bjarne Stroustrup (Developer of the C++ language)
*
* Private Data:
*   tempInput: Holds temporary input from the input method.
*   sentinelCheck: Boolean value that determines if loop continues to run.
*   list: Holds the list
* Methods:
*   void hello(): Greeting message
*   String inputMethod(): Generalized input method
*   void setInput(): Calls generalized input method to receive postfix string.
*   */
import java.io.*;
public class Postfix {
    private String tempInput;
    private boolean sentinelCheck = true;
    private ListStruct list = new ListStruct();

    private void hello() {
        System.out.println("Postfix Calculator");
    }
    private String inputMethod(String prompt) throws IOException {
        InputStreamReader reader = new InputStreamReader(System.in);
        BufferedReader input = new BufferedReader(reader);

        System.out.print(prompt + ": ");
        return input.readLine();
    }

    public void setInput() throws IOException {
        do {
            System.out.println("Type exit to exit.");
            tempInput = inputMethod("Enter calculation").toUpperCase();
            if(tempInput.equals("")) {
                System.out.println("Invalid syntax. Please try again");
            }
            if(tempInput.equals("EXIT")) {
                sentinelCheck = false;
            }
        } while(tempInput.equals(""));
    }

    public String getInput() {
        return tempInput;
    }

    public void postfixEval(String s) {
        char tempChar;
        for (int c=0;c<s.length();c++) {
            tempChar = s.charAt(c);

            if(Character.isDigit(tempChar)) {
                list.push(Integer.parseInt(Character.toString(tempChar)));
            }

            int temp1, temp2, total;
            switch(tempChar) {
                case '*': //multiply
                    temp1 = list.pop();
                    temp2 = list.pop();
                    total = temp1*temp2;
                    list.push(total);
                    break;
                case '+': //add
                    temp1 = list.pop();
                    temp2 = list.pop();
                    total = temp1 + temp2;
                    list.push(total);
                    break;
                case '-': //subtract
                    temp1 = list.pop();
                    temp2 = list.pop();
                    total = temp2 - temp1;
                    list.push(total);
                    break;
                case '/': //divide
                    temp1 = list.pop();
                    temp2 = list.pop();
                    try {
                        total = temp2 / temp1;
                        list.push(total);
                    }
                    catch(ArithmeticException e) {
                        total = -Integer.MAX_VALUE; // THAT'S WHAT YOU GET FOR DIVIDING BY ZERO. >.<
                        list.push(total);
                        System.out.println("WHY U DIVIDE BY ZERO");
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private String formatOutput() {
        return tempInput + " = " + list.peekTop();
    }

    public void callCommand() throws IOException {
        hello();
        do {
            setInput();
            if(sentinelCheck) {
                postfixEval(getInput());
                System.out.println(formatOutput());
                System.out.println();
            }
        } while(sentinelCheck);
        System.out.println("Goodbye.");
    }

    public static void main(String args[]) throws IOException {
        Postfix derp = new Postfix();
        derp.callCommand();
    }
}