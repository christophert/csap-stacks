/*
 * Original work Copyright (c) 2014 TranIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Project: csap-stacks
 * File: Node.java
 */
/*
*    Private Data:
     *  int value: holds the value of the node.
     *  Node next: holds the pointer to the next node
     * Static:
     *  None
     * Methods:
     *  getValue, getNext: get respective values
     *  setValue, setNext: set respective values
 */

public class Node {
    private int value;
    private Node next;
    public Node(int initValue, Node initNext) {
        value = initValue;
        next = initNext;
    }

    public int getValue() {
        return value;
    }

    public Node getNext() {
        return next;
    }

    public void setValue(int theNewValue) {
        value = theNewValue;
    }

    public void setNext(Node theNewNext) {
        next = theNewNext;
    }
}
