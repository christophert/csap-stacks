/*
 * Original work Copyright (c) 2014 TranIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Project: csap-stacks
 * File: ListStruct.java
 */
/*
* ListStruct
* Private Data:
*   Node head: holds the head node
* Methods:
*   push: pushes node onto top of stack.
*   pop: deletes and returns node at top of stack
*   peekTop: returns node at top of stack
*   isEmpty: checks if list is empty
*   */

public class ListStruct {
    public ListStruct() {}
    private Node head;
    public boolean isEmpty() {
        if(head == null)
            return true;
        return false;
    }

    public int push(int d) {
        Node temp = new Node(d, null);
        temp.setNext(head);
        head = temp;
        return 200;
    }

    public int pop() {
        Node temp = head;
        head = head.getNext();

        return temp.getValue();
    }

    public int peekTop() {
        return head.getValue();
    }
}
